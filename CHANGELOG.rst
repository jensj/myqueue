Changelog
=========

See what's new in MyQueue here:

    https://myqueue.readthedocs.io/releasenotes.html
